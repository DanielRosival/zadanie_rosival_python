import operator
def eval_expr(expr):
	listOfCharacters = expr.split()
	operators = {"+" : operator.add, "-" : operator.sub, "*" : operator.mul, "/" : operator.floordiv}
	container = []
	for char in listOfCharacters:
		if(char.isdigit()):
			container.append(int(char))
		else:
			aux = operators[char](container[-2],container[-1])
			container.pop(-1)
			container.pop(-1)
			container.append(aux)
	return container[-1]

def eval_expr(expr, d={}):
	listOfCharacters = expr.split()
	operators = {"+" : operator.add, "-" : operator.sub, "*" : operator.mul, "/" : operator.floordiv}
	container = []
	for char in listOfCharacters:
		if(char.isdigit()):
			container.append(int(char))
		elif(char in d):
			container.append(d[char])
		else:
			aux = operators[char](container[-2],container[-1])
			container.pop(-1)
			container.pop(-1)
			container.append(aux)
	return container[-1]

def to_infix(expr):
	listOfCharacters = expr.split()
	ret = []
	for char in listOfCharacters:
		if(char.isdigit()):
			ret.append(char)
		else:
			aux = " ( " + ret[-2] + " " + char + " " +  ret[-1] + " ) "
			ret.pop(-1)
			ret.pop(-1)
			ret.append(aux)
	return " ".join(ret)

def to_postfix(expr):
	listOfCharacters = expr.split()
	ret = []
	stack = []
	operators = ["+","-","*","/"]
	for char in listOfCharacters:
		if (char.isdigit()):
			ret.append(char)
		elif (char == "("):
			stack.append(char)
		elif (char == ")"):
			aux = stack.pop()
			while aux!="(":
				ret.append(aux)
				aux = stack.pop()
		elif (char in operators):
			stack.append(char)
	while stack:
		ret.append(stack.pop())
	return " ".join(ret)



